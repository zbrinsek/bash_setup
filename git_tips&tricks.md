Create repo from folder
``` bash
git init
git add .
git commit -m "First commit"
git remote add origin <remote repository URL>
git remote -v
git push origin master

```

Add submodule with branch
``` bash
git submodule add -b <branch> <repo url> <path>
#git submodule add -b devel git@bitbucket.org:user/repo.git
```

Remove submodule
``` bash
git submodule deinit <path_to_submodule>

git rm <path_to_submodule>

git commit-m "Removed submodule "

rm -rf .git/modules/<path_to_submodule> 

```

```
chmod -x <file>
git update-index --chmod=+x <file>
chmod +x <file>

```
