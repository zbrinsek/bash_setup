#!/bin/bash

alias adda=add_alias


alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."

alias s="source devel/setup.bash"
alias ph="cat  ~/.bash_aliases | grep alias | cut -f2- -d ' ' "

alias nb="nano ~/.bash_aliases ; source ~/.bash_aliases"
alias per="printenv | grep ROS"
alias u="source ~/.bash_aliases"
alias .-="cd -"
alias ,="ps1"
alias c="catkin_make"
alias uh="export ROS_HOSTNAME=$(hostname -I | awk '{print $1}')"
alias ml="mon launch"
alias tf="rosrun rqt_tf_tree rqt_tf_tree"
alias md="show_md"

#alias Ctrl + A        Go to the beginning of the line you are currently typing on
#alias Ctrl + E        Go to the end of the line you are currently typing on

#alias Ctrl + W        Delete the word before the cursor
#alias Ctrl + K        Clear the line after the cursor

#alias Alt + F         Move cursor forward one word on the current line
#alias Alt + B         Move cursor backward one word on the current line

#alias Ctrl + Y	       Restore text

#alias udevadm monitor
#alias udevadm info -a -n /dev/...
#alias sudo ip route add 172.17.0.0/16 via 10.123.70.13 dev eth0
