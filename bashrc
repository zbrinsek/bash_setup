function color_my_prompt {
   local __user_and_host="\[\033[01;32m\]\u@\h"
   local __cur_location="\[\033[01;34m\]\w"
   local __git_branch_color="\[\033[31m\]"
   #local __git_branch="\ruby -e \"print (%x{git branch 2> /dev/null}.grep(/^\*/).first || '').gsub(/^\* (.+)$/, '(\1) ')\"\"
   local __git_branch='`git branch 2> /dev/null | grep -e ^* | sed -E  s/^\\\\\*\ \(.+\)$/\(\\\\\1\)\ /`'
   local __prompt_tail="\[\033[35m\]$"
   local __last_color="\[\033[00m\]"
   export long_PS1="$__user_and_host $__cur_location $__git_branch_color$__git_branch$__prompt_tail$__last_color "
}
color_my_prompt

function _tmux_list () {
	tmux ls 2>/dev/null | cut -f1 -d ':' | tr '\n' ' '
}

#complete -F _tmux_list ta 

complete -W "$(_tmux_list) " ta

alias ta="complete -W \"$(_tmux_list) \" ta; tmux a -t"

function add_alias() {
	echo alias $1=\"$2\" >> ~/.bash_aliases
	source ~/.bash_aliases
}

function show_md() {
	pandoc $1 |  w3m -T text/html
}

function ps1() {
	if [ $PS1_flag == "long" ] ; then
		export PS1=$short_PS1	
		export PS1_flag="short"
	else
		export PS1=$long_PS1
		export PS1_flag="long"
	fi
}


bind '"\e[5~": history-search-backward'
bind '"\e[6~": history-search-forward'
bind '"\e[1;5C": forward-word'
bind '"\e[1;5D": backward-word'

export short_PS1="\[\033[01;32m\]\u@\h \[\033[00m\]"
export PS1_flag=long
export PS1=$long_PS1
export ROS_HOSTNAME=$(hostname -I | awk '{print $1}')
	
export ROS_LANG_DISABLE="genlisp:geneus:gennodejs"
