#!/bin/bash
read -p "Install [b]asic [a]dvance:" install
sudo apt install tmux pandoc w3m
cp bashrc ~/.user_bashrc
cp bash_aliases ~/.bash_aliases
cp tmux_basic.conf ~/.tmux.conf
cp nanorc ~/.nanorc
echo "source ~/.user_bashrc" >> ~/.bashrc
echo "source ~/.bashrc" >> ~/.bash_profile

if [  "$install" = "a" ]; then
	cp tmux.conf ~/.tmux.conf
	git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
	git submodule update --init
	mkdir -p ~/tmux/plugins
	cp -r tmux-resurrect ~/tmux/plugins/
	echo "Press prefix + I (capital I, as in Install) to fetch the plugin."
fi

