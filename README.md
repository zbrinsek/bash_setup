#Bash setup
##Scripts and config files for setup bash enviroment
* Tmux config with help for keybindings
* Nano tab size = 2
* ssh config file example
* git tricks
* and many more

For install run *bash install.sh*


Maintainer: Ziga Brinsek *ziga AT brinsek DOT si*
